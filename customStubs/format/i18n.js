/**
 * SuiteScript format/i18n module
 *
 * @module N/format/i18n
 * @NApiVersion 2.x
 *
 */
define([], function(){
    
    /**
     * @namespace i18n
     */    
    var i18n = {};    
    
    var format = {};
N.format = format;
    format.i18n = i18n;
    
    /**
     * @exports N/format/i18n
     */
    return i18n;
});