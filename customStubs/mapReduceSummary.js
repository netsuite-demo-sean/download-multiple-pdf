/**
 * SuiteScript mapReduceSummary module
 *
 * @module N/mapReduceSummary
 * @NApiVersion 2.x
 *
 */
define([], function(){
    
    /**
     * @namespace mapReduceSummary
     */    
    var mapReduceSummary = {};    
    
    /**
     * Return a new instance of mapreduce.Summary
     * @class
     * @classdesc Used for accessing Map/Reduce job output and metadata.
     * @return {Summary}
     * @constructor
     *
     * @since 2015.2
     */    
    function Summary() {    
        
        /**
         * @name Summary#dateCreated
         * @type {Date} dateCreated - Time M/R job began running.
         * @readonly
         */        
        this.dateCreated = undefined;        
        /**
         * @name Summary#seconds
         * @type {Number} seconds - Total seconds elapsed while running.
         * @readonly
         */        
        this.seconds = undefined;        
        /**
         * @name Summary#usage
         * @type {Number} usage - Total usage points consumed while running.
         * @readonly
         */        
        this.usage = undefined;        
        /**
         * @name Summary#concurrency
         * @type {Number} concurrency - Maximum number of queues utilized at the same time while running.
         * @readonly
         */        
        this.concurrency = undefined;        
        /**
         * @name Summary#yields
         * @type {Number} yields - Total number of times yielding the queue while running.
         * @readonly
         */        
        this.yields = undefined;        
        /**
         * @name Summary#output#iterator()
         * @type {Iterator} iterator - Iterator which provides keys and values written as output during the REDUCE phase.
         *      <pre> summary.output.iterator().each(function(key, value){...}); </pre>
         * @readonly
         */        
        this.output = undefined;        
        /**
         * @name Summary#inputSummary
         * @type {InputSummary} inputSummary - Stats about the INPUT stage.
         */        
        this.inputSummary = undefined;        
        /**
         * @name Summary#mapSummary
         * @type {mapSummary} mapSummary - Stats about the MAP stage.
         */        
        this.mapSummary = undefined;        
        /**
         * @name Summary#reduceSummary
         * @type {reduceSummary} reduceSummary - Stats about the REDUCE stage.
         */        
        this.reduceSummary = undefined;        
        /**
         * @returns {string}
         */        
        this.toString = function() {};        
        
        /**
         * JSON.stringify() implementation.
         */        
        this.toJSON = function() {};        
    }    
    
    /**
     * Return a new instance of mapreduce.InputSummary
     * @class
     * @classdesc Used for accessing Map/Reduce INPUT stage metadata.
     * @return {InputSummary}
     * @constructor
     *
     * @since 2015.2
     */    
    function InputSummary() {    
        
        /**
         * @name InputSummary#dateCreated
         * @type {Date} dateCreated - Time M/R INPUT stage began running.
         * @readonly
         */        
        this.dateCreated = undefined;        
        /**
         * @name InputSummary#seconds
         * @type {Number} seconds - Total seconds elapsed while during the INPUT stage.
         * @readonly
         */        
        this.seconds = undefined;        
        /**
         * @name InputSummary#usage
         * @type {Number} usage - Total usage points consumed during the INPUT stage.
         * @readonly
         */        
        this.usage = undefined;        
        /**
         * @name InputSummary#error
         *  @type {SuiteScriptError} error - Serialized error is thrown out of getInputData() - if applicable
         *      <pre> var inputError = summary.input.error; </pre>
         * @readonly
         */        
        this.error = undefined;        
        /**
         * @returns {string}
         */        
        this.toString = function() {};        
        
        /**
         * JSON.stringify() implementation.
         */        
        this.toJSON = function() {};        
    }    
    
    /**
     * Return a new instance of mapreduce.MapSummary
     * @class
     * @classdesc Used for accessing Map/Reduce MAP stage metadata.
     * @return {MapSummary}
     * @constructor
     *
     * @since 2015.2
     */    
    function MapSummary() {    
        
        /**
         * @name MapSummary#dateCreated
         * @type {Date} dateCreated - Time MAP stage began running.
         * @readonly
         */        
        this.dateCreated = undefined;        
        /**
         * @name MapSummary#seconds
         * @type {Number} seconds - Total seconds elapsed while running MAP stage.
         * @readonly
         */        
        this.seconds = undefined;        
        /**
         * @name MapSummary#usage
         * @type {Number} usage - Total usage points consumed while running MAP stage.
         * @readonly
         */        
        this.usage = undefined;        
        /**
         * @name MapSummary#concurrency
         * @type {Number} concurrency - Maximum number of queues utilized at the same time while running MAP stage.
         * @readonly
         */        
        this.concurrency = undefined;        
        /**
         * @name MapSummary#yields
         * @type {Number} yields - Total number of times yielding the queue while running MAP stage.
         * @readonly
         */        
        this.yields = undefined;        
        /**
         * @name MapSummary#keys#iterator
         * @type {Iterator} iterator - Iterator which provides input keys processed during the MAP phase.
         *      <pre> summary.map.keys.iterator.each(function(key){...}); </pre>
         * @readonly
         */        
        this.keys = undefined;        
        /**
         * @name MapSummary#errors#iterator
         * @type {Iterator} iterator - Iterator which provides errors thrown during the MAP phase.
         *      <pre> summary.map.errors.each(function(key, value){...}); </pre>
         * @readonly
         */        
        this.errors = undefined;        
        /**
         * @returns {string}
         */        
        this.toString = function() {};        
        
        /**
         * JSON.stringify() implementation.
         */        
        this.toJSON = function() {};        
    }    
    
    /**
     * Return a new instance of mapreduce.ReduceSummary
     * @class
     * @classdesc Used for accessing Map/Reduce REDUCE stage metadata.
     * @return {ReduceSummary}
     * @constructor
     *
     * @since 2015.2
     */    
    function ReduceSummary() {    
        
        /**
         * @name ReduceSummary#dateCreated
         * @type {Date} dateCreated - Time REDUCE stage began running.
         * @readonly
         */        
        this.dateCreated = undefined;        
        /**
         * @name ReduceSummary#seconds
         * @type {Number} seconds - Total seconds elapsed while running REDUCE stage.
         * @readonly
         */        
        this.seconds = undefined;        
        /**
         * @name ReduceSummary#usage
         * @type {Number} usage - Total usage points consumed while running REDUCE stage.
         * @readonly
         */        
        this.usage = undefined;        
        /**
         * @name ReduceSummary#concurrency
         * @type {Number} concurrency - Maximum number of queues utilized at the same time while running REDUCE stage.
         * @readonly
         */        
        this.concurrency = undefined;        
        /**
         * @name ReduceSummary#yields
         * @type {Number} yields - Total number of times yielding the queue while running REDUCE stage.
         * @readonly
         */        
        this.yields = undefined;        
        /**
         * @name ReduceSummary#keys#iterator
         * @type {Iterator} iterator - Iterator which provides input keys processed during the REDUCE phase.
         *      <pre> summary.reduce.iterator.keys.each(function(key){...}); </pre>
         * @readonly
         */        
        this.keys = undefined;        
        /**
         * @name ReduceSummary#errors#iterator
         * @type {Iterator} iterator - Iterator which provides errors thrown during the REDUCE phase.
         *      <pre> summary.reduce.errors.iterator().each(function(key, value){...}); </pre>
         * @readonly
         */        
        this.errors = undefined;        
        /**
         * @returns {string}
         */        
        this.toString = function() {};        
        
        /**
         * JSON.stringify() implementation.
         */        
        this.toJSON = function() {};        
    }    
    
    N.mapReduceSummary = mapReduceSummary;
    
    /**
     * @exports N/mapReduceSummary
     */
    return mapReduceSummary;
});