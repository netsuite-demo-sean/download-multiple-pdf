/**
 * SuiteScript Form Record Bootstrap Module
 *
 * @module N/currentRecordBootstrap
 * @suiteScriptVersion 2.x
 *
 */
define([], function(){
    
    /**
     * @namespace currentRecordBootstrap
     */    
    var currentRecordBootstrap = {};    
    
    /**
     * Return {Record} [singleton DR-based current record object, if one was already created by a prior call to
     * getModuleInstance(), otherwise, returnes undefined]
     */    
    function getCurrentRecord() {    
    }    
    
    /**
     * Create current record using promise
     *
     * @return {Record|SuiteScriptError}
     */    
    function getModuleInstance() {    
    }    
    
    N.currentRecordBootstrap = currentRecordBootstrap;
    
    /**
     * @exports N/currentRecordBootstrap
     */
    return currentRecordBootstrap;
});