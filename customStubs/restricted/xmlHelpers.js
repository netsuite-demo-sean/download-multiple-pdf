/**
 * SuiteScript module
 *
 * @private
 * @module N/restricted/xmlHelpers
 * @NApiVersion 2.x
 */
define([], function(){
    
    /**
     * @namespace xmlHelpers
     */    
    var xmlHelpers = {};    
    
    var restricted = {};
N.restricted = restricted;
    restricted.xmlHelpers = xmlHelpers;
    
    /**
     * @exports N/restricted/xmlHelpers
     */
    return xmlHelpers;
});