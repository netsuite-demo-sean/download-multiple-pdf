/**
 * SuiteScript module
 *
 * @private
 * @module N/restricted/ajaxHelpers
 * @NApiVersion 2.x
 */
define([], function(){
    
    /**
     * @namespace ajaxHelpers
     */    
    var ajaxHelpers = {};    
    
    var restricted = {};
N.restricted = restricted;
    restricted.ajaxHelpers = ajaxHelpers;
    
    /**
     * @exports N/restricted/ajaxHelpers
     */
    return ajaxHelpers;
});