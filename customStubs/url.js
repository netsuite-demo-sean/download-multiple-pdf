/**
 * SuiteScript module
 *
 * @module N/url
 * @NApiVersion 2.x
 *
 */
define([], function(){
    
    /**
     * @namespace url
     */    
    var url = {};    
    
    /**
     * @param {Object} options
     * @param {string} options.recordType
     * @param {string} options.recordId
     * @param {boolean} options.isEditMode
     * @param {Object} options.params Per url.format({query
     *
     * @return {String} url
     *
     * @since 2015.1
     */    
    url.resolveRecord = function(options) {};    
    
    /**
     *
     * @param {Object} options
     * @param {string} options.id
     * @param {Map} options.params (optional) url parameters
     *
     * @return {String} url
     *
     * @since 2015.1
     */    
    url.resolveTaskLink = function(options) {};    
    
    /**
     * @param {Object} options
     * @param {string} options.scriptId
     * @param {string} options.deploymentId
     * @param {boolean} options.returnExternalUrl
     * @param {Object} options.params Per url.format({query
     *
     * @return {String} url
     *
     * @since 2015.1
     */    
    url.resolveScript = function(options) {};    
    
    /**
     * @param {Object} options
     * @param {string} options.hostType
     * @param {string} options.accountId
     *
     * @return {String} domain
     *
     * @since 2017.1
     */    
    url.resolveDomain = function(options) {};    
    
    /**
     * @param {Object} options
     * @param {string} options.domain
     * @param {Object} options.params query string data parameters as an object
     *
     * @return {String} url
     *
     * @since 2015.1
     */    
    url.format = function(options) {};    
    
    N.url = url;
    
    /**
     * @exports N/url
     */
    return url;
});