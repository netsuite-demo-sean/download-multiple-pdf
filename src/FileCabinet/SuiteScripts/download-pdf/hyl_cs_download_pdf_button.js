define([],

  function () {

    /**
     * Module Description...
     *
     * @type {Object} module-name
     *
     * @copyright 2020 HYL
     * @author Sean LIn <seanlin816@gmail.com>
     *
     * @NApiVersion 2.0
     * @NModuleScope Public

     * @NScriptType ClientScript
     */
    var exports = {};

    /**
     * <code>pageInit</code> event handler
     *
     * @gov XXX
     *
     * @param context
     *    {Object}
     * @param context.mode
     *    {string} The access mode of the current record. Will be one of
     *            <ul>
     *            <li>copy</li>
     *            <li>create</li>
     *            <li>edit</li>
     *            </ul>
     * @param context.currentRecord {CurrentRecord} The record in context
     *
     * @return {void}
     *
     * @static
     * @function pageInit
     */
    function pageInit(context) {
      // TODO
    }

    // 真正的按鈕行為
    function downloadButtonClicked(){

    }


    // 至少需要一個 client script 的 default action 讓 client script 可以在 suitelet 的 page 上使用
    exports.pageInit = pageInit;
    return exports;
  });
